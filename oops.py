#class attribute or data member
pi=3.14
#class method or member func
def area(self,r):
    return self.pi*r*r
def parameter(self,r):
    return 2*self.pi*r

#create object
ob1=area() 
ob2=parameter()   
print(ob1.pi)
print(ob1.circle(100))    

#input from user
radius=int(input('enter circle radius'))
#print('Area of circle is{}'.format(ob1.area(radius)))
print('Area of circle is{}'.format(ob1.parameter(radius)))
