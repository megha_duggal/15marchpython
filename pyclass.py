class myClass:
    a='class attribute'
    b='Another att'

    def intro(self,name,color):
        return 'color of {} is {}'.format(name,color) 

ob=myClass()
print(ob.a)
ob1=myClass()    

obj1=myClass()
print(ob.b)
#or another way to access
print(ob.__class__.b) 
ent1=ob.intro('parrot','green')
print(ent1)
ent2=ob1.intro('cow','white')
print(ent2)